#! /bin/csh 
# CMC Mar 1, 2021: get tmax and tmin from NRCC server and convert txt files to netcdf 
# ----
# CMC Mar 27, 2019: add catncfiles.v2.csh to concatenate daily fields into a single file.
#                   The previous version does it daily but it did not check with the file is missing.
#                   We have fixed it with catncfiles.v2.csh. 
# CMC Updated Jan 17, 2018 
# 

set path = ($path /usr/local/bin)
set path = ($path /home/exclude/matlab/R2016a/bin)
#--------------
set exepath = /home/cmc542/2021/pheno_fcst/scripts/getNRCC

set tmon = (31 28 31 30 31 30 31 31 30 31 30 31) 
cd $exepath

set yrini = 2001
set yrend = 2002 

#---------------

foreach yy (`awk -v T1=$yrini -v TN=$yrend 'BEGIN{for(i=T1;i<=TN;i++){if (i<10) i=0i;print i}}'`)
  set leap4 = `expr $yy  % 4` 
  set leap100 = `expr $yy  % 100` 
  set leap400 = `expr $yy  % 400` 
  if ( $leap4 == 0 ) then 
      if ($leap100 != 0 ) then 
        set tmon = (31 29 31 30 31 30 31 31 30 31 30 31) 
      else
         if ($leap400 == 0 ) then 
              set tmon = (31 29 31 30 31 30 31 31 30 31 30 31) 
         endif 
     endif 
  endif 

  foreach mm ( 01 02 03 04 05 06 07 08 09 10 11 12 )
#    foreach dy ( 01 02 03 04 )
     foreach dy ( `awk -v TM=$tmon[$mm] 'BEGIN{for(i=1;i<=TM;i++){if (i<10) i=0i;print i}}'` )
     echo $dy 
     exit 
#------
set x_yymmdd = `date +%Y%m%d` 
#set x_yymmdd = '20200202'
set x_yymmdd = ${yy}${mm}${dy}

set y_yymmdd = `/home/cmc542/2016/ATLAS/getdata/next_date.csh $x_yymmdd -1` 

set iyr = `echo $y_yymmdd | awk '{print substr($1,1,4)}'` 
set imo = `echo $y_yymmdd | awk '{print substr($1,5,2)}'` 
set idy = `echo $y_yymmdd | awk '{print substr($1,7,2)}'` 


#
#set sdate = '2017-01-02'
#set sdate = '2017-01-03'

 set sdate = $iyr"-"$imo"-"$idy
 echo $sdate 

#-------------
 set yyyy =  `echo $sdate | awk '{a=split($1,b,"-"); print b[1]}'`
 set mm =  `echo $sdate | awk '{a=split($1,b,"-"); print b[2]}'`
 set dd =  `echo $sdate | awk '{a=split($1,b,"-"); print b[3]}'`
#-------------
 echo "##################" 
 echo "1. GETTING data ... " $sdate 
 echo "##################"  
 
  python grid_tmax_daily.py  $sdate
  python grid_tmin_daily.py  $sdate

#
# remove simbols from file 
# 
 set f1 = 'grid_data_tmax.txt'
 set f2 = 'grid_data_tmax.NRCC.txt'

 sed s/","/" "/g $f1 |\
 sed s/"]"/" "/g |\
 sed s/"\["/" "/g  > $f2

 set f1 = 'grid_data_tmin.txt'
 set f2 = 'grid_data_tmin.NRCC.txt'

 sed s/","/" "/g $f1 |\
 sed s/"]"/" "/g |\
 sed s/"\["/" "/g  > $f2
#

#-------------# 
# regrid files 
# output: tmax.nc 
#         tmin.nc 
 echo "##################" 
 echo "2. CONVERT txt2nc files ... " 
 echo "##################" 

 matlab -nodisplay -r "sdate='$sdate';txt2nc_NRCC;exit"

 echo "##################" 
 echo "3. GETTING the sites ..."
 echo "##################" 

# Rscript --vanilla sites_extract.R tmax.nc tmax tmax.sites.RDS 
# Rscript --vanilla sites_extract.R tmin.nc tmin tmin.sites.RDS 

# Rscript --vanilla sites_extract.R tmax.nc tmax tmax.sites.${sdate}.RDS 

#exit 

mv tmin.nc  tmin_Aday_NRCC_${sdate}_dy.nc   
mv tmax.nc  tmax_Aday_NRCC_${sdate}_dy.nc   

#exit 
#---- 
     end
  end 
end 

# make all days in 1 file
