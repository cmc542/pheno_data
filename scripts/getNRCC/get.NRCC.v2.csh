#! /bin/csh 
# CMC Mar 1, 2021: get tmax and tmin from NRCC server and convert txt files to netcdf 
# ----
# CMC Mar 27, 2019: add catncfiles.v2.csh to concatenate daily fields into a single file.
#                   The previous version does it daily but it did not check with the file is missing.
#                   We have fixed it with catncfiles.v2.csh. 
# CMC Updated Jan 17, 2018 
# 

set path = ($path /usr/local/bin)
set path = ($path /home/exclude/matlab/R2016a/bin)
#------
set exepath = /home/cmc542/2021/pheno_fcst/scripts/getNRCC

cd $exepath
#------
set x_yymmdd = `date +%Y%m%d` 
set x_yymmdd = '20200202'

set y_yymmdd = `/home/cmc542/2016/ATLAS/getdata/next_date.csh $x_yymmdd -1` 

set iyr = `echo $y_yymmdd | awk '{print substr($1,1,4)}'` 
set imo = `echo $y_yymmdd | awk '{print substr($1,5,2)}'` 
set idy = `echo $y_yymmdd | awk '{print substr($1,7,2)}'` 


#
#set sdate = '2017-01-02'
#set sdate = '2017-01-03'

 set sdate = $iyr"-"$imo"-"$idy
 echo $sdate 

#-------------
 set yyyy =  `echo $sdate | awk '{a=split($1,b,"-"); print b[1]}'`
 set mm =  `echo $sdate | awk '{a=split($1,b,"-"); print b[2]}'`
 set dd =  `echo $sdate | awk '{a=split($1,b,"-"); print b[3]}'`
#-------------
 echo "##################" 
 echo "1. GETTING data ... " $sdate 
 echo "##################"  
 
# python grid_tmax_daily.py  $sdate
# python grid_tmin_daily.py  $sdate

#
# remove simbols from file 
# 
 set f1 = 'grid_data_tmax.txt'
 set f2 = 'grid_data_tmax.NRCC.txt'

 sed s/","/" "/g $f1 |\
 sed s/"]"/" "/g |\
 sed s/"\["/" "/g  > $f2

 set f1 = 'grid_data_tmin.txt'
 set f2 = 'grid_data_tmin.NRCC.txt'

 sed s/","/" "/g $f1 |\
 sed s/"]"/" "/g |\
 sed s/"\["/" "/g  > $f2
#

#-------------# 
# regrid files 
# output: tmax.nc 
#         tmin.nc 
 echo "##################" 
 echo "2. CONVERT txt2nc files ... " 
 echo "##################" 

 matlab -nodisplay -r "sdate='$sdate';txt2nc_NRCC;exit"

 echo "##################" 
 echo "3. GETTING the sites ..."
 echo "##################" 

 Rscript --vanilla sites_extract.R tmax.nc tmax tmax.sites.RDS 
 Rscript --vanilla sites_extract.R tmin.nc tmin tmin.sites.RDS 

exit 

mv tmin.nc  tmin_Aday_NRCC_${sdate}_dy.nc   
mv tmax.nc  tmax_Aday_NRCC_${sdate}_dy.nc   

exit 
#---- 
