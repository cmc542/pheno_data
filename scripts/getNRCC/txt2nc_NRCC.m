%---
% CMC Mar 1, 2021: convert NRCC txt files into netcdf 
%---
 infile =  'grid_data_tmax.NRCC.txt'; 
 xdata = load(infile); 
 latmax = xdata(:,1); 
 lonmax = xdata(:,2); 
 varmax = xdata(:,3); 
%---
 infile =  'grid_data_tmin.NRCC.txt'; 
 xdata = load(infile); 
 latmin = xdata(:,1); 
 lonmin = xdata(:,2); 
 varmin = xdata(:,3); 
%---
 data_fill=-999; 
%--
%--
it=1; 
for i=1:624
    for j=1:1416

        var2Dmx(i,j) = varmax(it); 
        lat2Dmx(i,j) = latmax(it);
        lon2Dmx(i,j) = lonmax(it);

        var2Dmn(i,j) = varmin(it); 
        lat2Dmn(i,j) = latmin(it);
        lon2Dmn(i,j) = lonmin(it);

	it=it+1; 
    end 
end 
lat1Dmx=lat2Dmx(:,1); 
lon1Dmx=lon2Dmx(1,:); 
%--
lat1Dmn=lat2Dmn(:,1); 
lon1Dmn=lon2Dmn(1,:); 
%--
var2Dmn( find(var2Dmn==-999) ) = NaN; 
var2Dmx( find(var2Dmx==-999) ) = NaN; 

%--- TO FILE ---
% 
       data_fill = single(data_fill);
       vunits = 'deg_F';
%      sdate =  '2002-12-12'
       tunit = 'days   ';
       xtmp = strcat(sdate,' 00:00:00');
       time_ini = ['days since ',xtmp];
      [nrow ncol ntime] = size(var2Dmn);
       t = [0:ntime-1];
       varname = strtrim('tmin');
       outfile='tmin.nc'
       wrnc3dll_x(outfile,lat1Dmn,lon1Dmn,t,var2Dmn',tunit,time_ini,data_fill,varname,vunits);
       varname = strtrim('tmax');
       outfile='tmax.nc'
       wrnc3dll_x(outfile,lat1Dmx,lon1Dmx,t,var2Dmx',tunit,time_ini,data_fill,varname,vunits);
return 
