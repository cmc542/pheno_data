import sys, urllib, urllib2

try:
	import json
except ImportError:
	import simplejson as json

#--------------------------------------------------------------------------------------

#call to monthly get grid data 
# for Carlos

#--------------------------------------------------------------------------------------
#for eachArg in sys.argv:   
#    prtint eachArg 
#    print  sys.argv[0]
for eachArg in sys.argv:   
        print eachArg
cdate = eachArg
print cdate



File1 = open('grid_data_tmin.txt','w')
#File1.write('Lat, Lon, 1/2012,2/2012,3/2012 \n')  
#params = {'bbox': '-125,20,-55,65' , 'grid':'1', 'meta':'ll','sdate' : '2013-12-01', 'edate': '2013-12-01', 'elems' : [{'name':'mint','interval':'dly','duration':'dly','reduce':'mean'}]}  
params = {'bbox': '-125,20,-55,65' , 'grid':'1', 'meta':'ll','sdate' : cdate, 'edate': cdate, 'elems' : [{'name':'mint','interval':'dly','duration':'dly','reduce':'mean'}]}  



acis_url = 'http://data.rcc-acis.org/GridData'  
req = urllib2.Request(acis_url,urllib.urlencode({'params':json.dumps(params)}), {'Accept':'application/json'})
response = urllib2.urlopen(req)
f = json.loads(response.read())						#use json format to get ll, elev, etc


latt = f['meta']['lat'] #gives list of lists [[#,#,#...],[#,#,#...]...]
long = f['meta']['lon']
data = f['data'] #gives [date,[#,#,#...]] [date,[#,#,#...]]

x = 0
while x < len(latt):  # number of grid cells

	latt2 = latt[x] #gives list of lats [#,#,#...]
	long2 = long[x]


	y = 0
	while y < len(latt2):  # number of grid points in each grid cell

		lat = latt2[y]
		lon = long2[y]

		list = []

		z = 0
		while z< len(data):
			val = data[z][1][x][y]
			
			list.append(val)
			
			z = z + 1
			
		File1.write('%s, %s, %s \n' % (lat, lon, list))


		y = y + 1

	x = x + 1

File1.close()		
