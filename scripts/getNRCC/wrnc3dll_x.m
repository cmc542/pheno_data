% 
% Create NetCDF 
% 

function wrnc2d(ncfile,lat,lon,nwtime,wrvar,tunit,time_ini,data_fill,varname,vunits) 

%--whos lat lon nwtime wrvar data_fill 
wrvar (find( isnan(wrvar) == 1)) = data_fill; 

%----------------------------------% 
nca=netcdf.create(ncfile,'NC_SHARE');

% Define dimensions
nlat=max(size(lat)); 
nlon=max(size(lon)); 
ntime=max(size(nwtime)); 
%
lon_dim=netcdf.defDim(nca,'lon',nlon);
lat_dim=netcdf.defDim(nca,'lat',nlat);
tim_dim=netcdf.defDim(nca,'time',ntime);
%%
% Define variables and attributes
lon_id=netcdf.defVar(nca,'lon','double',lon_dim);
netcdf.putAtt(nca,lon_id,'long_name','Longitude');
netcdf.putAtt(nca,lon_id,'units','degrees_east');
netcdf.putAtt(nca,lon_id,'standard_name','lon');
%
lat_id=netcdf.defVar(nca,'lat','double',lat_dim);
netcdf.putAtt(nca,lat_id,'long_name','Latitude');
netcdf.putAtt(nca,lat_id,'units','degrees_north');
netcdf.putAtt(nca,lat_id,'standard_name','lat');
%
time_id=netcdf.defVar(nca,'time','double',tim_dim);
%netcdf.putAtt(nca,time_id,'units','hours since 1968-01-01 00:00:00');
%netcdf.putAtt(nca,time_id,'units','years since 1968-01-01 00:00:00');
%if(tunit=='years  ')
   netcdf.putAtt(nca,time_id,'units',time_ini);
%end
netcdf.putAtt(nca,time_id,'calendar','standard');
%

%data_id = netcdf.defVar(nca,'data','float',[lon_dim lat_dim tim_dim]);
data_id = netcdf.defVar(nca,varname,'float',[lon_dim lat_dim tim_dim]);

%netcdf.defVarFill(nca,data_id,false,-999);


%netcdf.putAtt(nca,data_id,'units','m');
%netcdf.putAtt(nca,data_id,'units','mm/month');

  netcdf.putAtt(nca,data_id,'units',vunits);

% netcdf.putAtt(nca,data_id,'_FillValue',data_fill);
% netcdf.putAtt(nca,data_id,strtrim('_FillValue'),data_fill);

% netcdf.putAtt(nca,data_id,strtrim('_FillValue'),data_fill);
  netcdf.putAtt(nca,data_id,strtrim('missing_value'),data_fill);

% xfalse  = '_FillValue';
% netcdf.putAtt(nca,data_id,strtrim(xfalse),data_fill);
% netcdf.defVarFill(nca,data_id,false,data_fill);
% netcdf.defVarFill(nca,data_id,false,-999.9);

  netcdf.endDef(nca);

% WRITE data
netcdf.putVar(nca,lon_id,lon);
netcdf.putVar(nca,lat_id,lat);
netcdf.putVar(nca,time_id,nwtime);
netcdf.putVar(nca,data_id,wrvar);
netcdf.reDef(nca);
netcdf.close(nca);
%
